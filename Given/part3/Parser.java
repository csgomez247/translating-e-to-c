/**
 * Parser.java                               
 * Parser class to ensure legal E program.   
 * All function definitions are preceded by  
 * their EBNF definitions                    
 **/
 
class Parser {
	
	private Token token; // Current token being processed
	private Scan scanner;
	private SymbolTable symTab;
	// Constructor, set scanner, begin scanning, and call program
	// Also check that nothing follows the end of file token
	public Parser(Scan scanner) {
		this.scanner = scanner;
		this.symTab = new SymbolTable();

		scan();					// Get first token
		program();				// Parse program
		
		// If last token is not end of file token, error
		// Syntax pulled from sample files
		if ( !match(TK.EOF) )
			error("junk after logical end of program");
	} // Parser()
	
	// Non-terminal token parser functions
	// program ::= block
	private void program() {
		// Program can only start in one way!
		block();
	} // program()
	
	// block ::= [declarations] statement_list
	private void block() {
		symTab.addFrame();
		if (match(TK.VAR))	// begin with declarations
			declarations();	// dat modularity though
		statement_list();	// No seriously, dat modularity
		symTab.popFrame();
	} // block()
	
	// declarations ::= "var" { id } "rav"
	private void declarations() {
		mustbe(TK.VAR);
		while(match(TK.ID))
		{
			if(symTab.tableContains(token) == -1 || symTab.tableContains(token) > 1)
				symTab.addVar(new Variable(token));

			else // symTab.tableContains(token) == 1
				System.err.println("variable " + token.string +
					" is redeclared on line " + token.lineNumber);
			scan();				// Eat up all the IDs
		}//while current token is an ID
		
		// Check against "RAV"
		mustbe(TK.RAV); 
	} // declarations()
	
	
	// statement_list ::= { statement }
	private void statement_list() {
		// Indefinite number of statements
		while(isStatement())
			statement();	
	} // statement_list
	
	// statement ::= assignment | if | do | fa
	private void statement() {
		
		// Check which statement to execute
		if      (match(TK.IF))    { eif(); }
		else if (match(TK.DO))    { edo(); }
		else if (match(TK.FA))    { fa(); }
		else if (match(TK.ID))    { assignment(); }
		else if (match(TK.PRINT)) { print(); }
		else
			error("statement");
	} // statement()
	
	// assignment ::= id ":=" expression
	private void assignment() {		
		// Only one form, really easy to do
		if(symTab.tableContains(token) == -1)
		{
			System.err.println("undeclared variable " + token.string +
				" on line " + token.lineNumber);
			System.exit(1);
		}//if
		mustbe(TK.ID);
		mustbe(TK.ASSIGN);
		expression();
	} // assignment()
	
	// print ::= "print" expression
	private void print() {
		mustbe(TK.PRINT);
		expression();
	} // print()
	
	// if ::= "if" guarded_commands "fi"
	// eif instead of if because if is a keyword in java
	private void eif() {
		mustbe(TK.IF);
		guarded_commands();
		mustbe(TK.FI);
	} // eif()
	
	// do ::= "do" guarded_commands "od"
	// edo instead of do because do is a keyword in java
	private void edo() {
		mustbe(TK.DO);
		guarded_commands();
		mustbe(TK.OD);
	} // edo()
	
	// fa ::= "fa" id ":=" expression "to" expression ["st" expression] commands "af"
	private void fa() {
		mustbe(TK.FA);

		if(symTab.tableContains(token) == -1)
		{
			System.err.println("undeclared variable " + token.string +
				" on line " + token.lineNumber);
			System.exit(1);
		}//if

		mustbe(TK.ID);
		mustbe(TK.ASSIGN);
		expression();
		mustbe(TK.TO);
		expression();
		
		// Can have EXACTLY ONE st class
		if (match(TK.ST))	// If there is an optional "st" clause
		{
			mustbe(TK.ST);
			expression();	// Parse expression following it
		} // if()
		
		commands();
		mustbe(TK.AF);
	} // fa()
	
	// guarded_commands ::= guarded_command { "[]" guarded_command } [ "else" commands ]
	private void guarded_commands() {
		guarded_command();
		
		// Can have INDEFINITEY guarded_commands
		while(match(TK.BOX)) {
			mustbe(TK.BOX);
			guarded_command();
		} // while()
		
		// Can have EXACTLY ONE else clause
		if (match(TK.ELSE)) {
			mustbe(TK.ELSE);
			commands();
		} // if()
	} // guarded_commands()
	
	// guarded_command ::= expression commands
	private void guarded_command() {
		expression();
		commands();
	} // guarded_command()
	
	// commands ::= "->" block
	private void commands() {
		mustbe(TK.ARROW);
		block();
	} // commands()
	
	// expression ::= simple [relop simple]
	private void expression() {
		simple();
		
		if (isRelop()) {
			relop();
			simple();
		}
	} // expression()
	
	// simple ::= term {addop term}
	private void simple() {
		term();
		
		while(isAddop()) {
			addop();
			term();
		} // while()
	} // simple()
	
	// term ::= factor { multop factor }
	private void term() {
		factor();
	
		while (isMultop()) {
			multop();
			factor();
		} // while()
	} // term()
	
	// factor ::= "(" expression ")" | id | number
	private void factor() {

		if (match(TK.LPAREN)) {
			mustbe(TK.LPAREN);
			expression();
			mustbe(TK.RPAREN);
		} // chew through expression
		else if (match(TK.ID))  { 
			if(symTab.tableContains(token) == -1)
			{
				System.err.println("undeclared variable " + token.string +
					" on line " + token.lineNumber);
				System.exit(1);
			}//if
			mustbe(TK.ID); }	
		else if (match(TK.NUM)) { mustbe(TK.NUM); }
		else                    { error("factor"); }
	} // factor()
	
	// relop ::= "=" | "<" | ">" | "/=" | "<=" | ">="
	private void relop() {
		// Match all possible values
		if (match(TK.EQ))      { mustbe(TK.EQ); }      
		else if (match(TK.LT)) { mustbe(TK.LT); } 
		else if (match(TK.GT)) { mustbe(TK.GT); }
		else if (match(TK.NE)) { mustbe(TK.NE); }
		else if (match(TK.LE)) { mustbe(TK.LE); }
		else if (match(TK.GE)) { mustbe(TK.GE); } 
		
		// Error if we don't have any of the above
		else { error("relop"); }
	} // relop()
	
	// addop ::= "+" | "-"
	private void addop() {			
		// Match all possible values
		if (match(TK.PLUS))       { mustbe(TK.PLUS);}       
		else if (match(TK.MINUS)) { mustbe(TK.MINUS); }
		else                      { error("addop"); }
	} // addop()
	
	// multop ::= "*" | "/"
	private void multop() {	
		// Match all possible values
		if (match(TK.TIMES))       { mustbe(TK.TIMES); }
		else if (match(TK.DIVIDE)) { mustbe(TK.DIVIDE); }
		else                       { error("multop"); }
	} // multop()
	
	//==============================================================//
	//                     UTILITY FUNCTIONS                       
	//==============================================================//
	
	// Check if current token is of given type
	// If it is, read next token
	private void mustbe(TK type) {
		if( ! match(type) ) {
			System.err.println( "mustbe: want " + type + ", got " + token);
			error( "missing token (mustbe)" );
		} // if()
		
		scan();
	} // mustbe()
	
	// When we reach an error, report the error and quit parsing
	// Pulled formatting from sample files
	private void error(String message) {
		System.err.println( "can't parse: line " + token.lineNumber + " " + message );
		System.exit(1);
	} // error()

	// Returns true if the current token is in the first set of statement
	private boolean isStatement() {
		if (token.kind != TK.ID) 
			if (token.kind != TK.PRINT) // print	
				if (token.kind != TK.IF)
					if (token.kind != TK.DO)
						if (token.kind != TK.FA)
							return false;
		
		return true;
	} // isStatement()
	
	// Returns true if the current token is a relop
	private boolean isRelop() {
		if (token.kind != TK.EQ)
			if (token.kind != TK.NE)
				if (token.kind != TK.LT)
					if (token.kind != TK.GT)
						if (token.kind != TK.LE)
							if (token.kind != TK.GE)
								return false;
		
		return true;
	} // isRelop()
	
	// Returns true if the current token is a multop
	private boolean isMultop() { 
		if (token.kind != TK.TIMES)
			if (token.kind != TK.DIVIDE)
				return false;
		
		return true;
	} // isMultop()
	
	private boolean isAddop() {
		if (token.kind != TK.PLUS)
			if (token.kind != TK.MINUS)
				return false;
		
		return true;
	} // isAddop()
	
	// Just so we don't have to call "scanner.scan" all the time
	private void scan() { 
		token = scanner.scan(); 
	} // scan()
	
	// Check that current token matches given ID
	private boolean match(TK type) { 
		return token.kind == type;
	} // match()
	
} // Parser()