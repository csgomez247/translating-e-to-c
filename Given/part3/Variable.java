import java.util.*;

/**
 * Representation of a variable.
 * Used in SymbolTable.java
 */
public class Variable  {

	private String name;	// Name of Variable
	private int uses;			// Store how many times the Variable is used
	private ArrayList<Integer> lineNumbers;	// Where it's used

	/**
	 * Constructor for a Variable object
	 * @param token initializes name, uses, and a starting line number
	 */
	public Variable(Token token) {
		this.name = token.string;
		uses = 1;
		lineNumbers = new ArrayList<Integer>();
		lineNumbers.add(token.lineNumber);
	} // Variable constructor

	/**
	 * Displays the name, number of uses, and locations of use
	 */
	public void displayInfo() {
		System.out.println("Name: " + name);
		System.out.println("Uses: " + uses);
		
		System.out.print(  "In:	  ");
		for(int i : lineNumbers) {
			System.out.print(lineNumbers.get(i));
		System.out.println();
		}//loop through lineNumbers
	} // displayInfo()

	/**
	 * Gives the name of the variable
	 * @return the String name of the variable
	 */	
	public String getName() {
		return name; 
	}//name

	/**
	 * Gives uses of a variable
	 * @return amount of uses
	 */
	public int getUses() {
		return uses; 
	}//uses

	/**
	 * Compares two Variable objects
	 * @param var varible to be compared
	 * @return true if the two objects' names are equal, false if not
	 */
	@Override
	public boolean equals(Object o) {

		if(o == null || !(o instanceof Variable))
			return false;
		if(o == this) 
			return true;
		Variable var = (Variable)o;
		return this.getName().equals(var.getName());
	}//equals

	/**
	 * String representation of a Variable
	 * @return Variable's name
	 */
	@Override
	public String toString() {
		return this.getName();
	}
} // Variable