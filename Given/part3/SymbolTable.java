/**
 * Symbol.java
 * Symbol table for e2c translator
 * Keeps each varlist in a stack
 */

import java.util.ArrayList;
import java.util.Stack;

/**
 * Symbol table for the E to C Translator.
 * Maintains the VarLists in a stack.
 */
public class SymbolTable {
	// Array of frames arranged as a stack
	private Stack<VarList> frames;
	
	/**
	 * Default, no argument constructor
	 */
	SymbolTable() { 
		frames = new Stack<VarList>();
		this.addFrame(); 
	}//no args contructor
	
	/**
	 * Creates a new frame
	 */
	public void addFrame() { 
		frames.push(new VarList()); 
	}//addFrame()
	
	/**
	 * Adds a variable to the VarList on top of the stack
	 * @param var variable to be added
	 */
	public void addVar(Variable var) {
		if(frames.empty())
			this.addFrame();
		(frames.peek()).add(var);
	}//addVar()

	/**
	 * Determines if frames is empty
	 * @return true if empty, false if not
	 */
	public boolean isEmpty() {
		return frames.empty();
	}

	/**
	 * Deletes the current frame
	 */
	public void popFrame() {
	 frames.pop(); 
	}//popFrame()

	/**
	 * Checks for the presence of a variable in the Symbol Table
	 * @param token variable name that is searched for
	 * @return 	-1 if Variable is not found, else returns 1-based depth level of Variable relative to top of stack
	 */
	public int tableContains(Token token) {
		VarList varlist = new VarList(new Variable(token));
		return frames.search(varlist);
	}//isInTable
} // SymbolTable()
 
