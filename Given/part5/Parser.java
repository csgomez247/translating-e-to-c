/**
 * Parser.java                               
 * Parser class to ensure legal E program.   
 * All function definitions are preceded by  
 * their EBNF definitions                    
 **/
 
class Parser {
	
	private Token token; // Current token being processed
	private Scan scanner;
	private SymbolTable symTab;
	// Constructor, set scanner, begin scanning, and call program
	// Also check that nothing follows the end of file token
	public Parser(Scan scanner) {
		this.scanner = scanner;
		this.symTab = new SymbolTable();

		scan();					// Get first token
		program();				// Parse program
		
		// If last token is not end of file token, error
		// Syntax pulled from sample files
		if ( !match(TK.EOF) )
			error("junk after logical end of program");
		
		// Output variable history at end of program parsing
		this.symTab.displayVariables();
	} // Parser()
	
	// Non-terminal token parser functions
	// program ::= block
	private void program() {
		// Program can only start in one way!
		System.out.println("#include <stdio.h>\n\n");
		System.out.println("int main() \n{");
		block();
		System.out.println("return 0;");
		System.out.println("}//main");
	} // program()
	
	// block ::= [declarations] statement_list
	private void block() {
		//System.out.println("\n{");
		symTab.addFrame();
		
		if (match(TK.VAR))	// begin with declarations
			declarations();	// dat modularity though
		statement_list();	// No seriously, dat modularity

		//System.out.println("}");
		symTab.popFrame();
	} // block()
	
	// declarations ::= "var" { id } "rav"
	private void declarations() {
		mustbe(TK.VAR);
		boolean redeclared = false;
		while(match(TK.ID))
		{
			redeclared = false;

			if(symTab.tableContains(token) == -1 || symTab.tableContains(token) > 1)
				symTab.addVar(new Variable(token, symTab.getNestingDepth()));

			else // symTab.tableContains(token) == 1
			{
				System.err.println("variable " + token.string +
					" is redeclared on line " + token.lineNumber);
				redeclared = true;
			}

			if(!redeclared)
				System.out.println("int x_" + token.string + " = -12345;");
			scan();				// Eat up all the IDs
		}//while current token is an ID
		
		// Check against "RAV"
		mustbe(TK.RAV); 
	} // declarations()
	
	
	// statement_list ::= { statement }
	private void statement_list() {
		// Indefinite number of statements
		while(isStatement())
			statement();	
	} // statement_list
	
	// statement ::= assignment | if | do | fa
	private void statement() {
		
		// Check which statement to execute
		if      (match(TK.IF))    { eif(); }
		else if (match(TK.DO))    { edo(); }
		else if (match(TK.FA))    { fa(); }
		else if (match(TK.ID))    { assignment(); }
		else if (match(TK.PRINT)) { print(); }
		else
			error("statement");
	} // statement()
	
	// assignment ::= id ":=" expression
	private void assignment() {		
		// Only one form, really easy to do
		if(symTab.tableContains(token) == -1)
		{
			System.err.println("undeclared variable " + token.string +
				" on line " + token.lineNumber);
			System.exit(1);
		}//if
		
		// Increment the count of assignments for this line
		symTab.incrementAssignment(token, symTab.getNestingDepth());

		System.out.print("x_" + token.string);
		mustbe(TK.ID);

		System.out.print(" = ");
		mustbe(TK.ASSIGN);
		expression();

		System.out.println(";");
	} // assignment()
	
	// print ::= "print" expression
	private void print() {
		mustbe(TK.PRINT);
		System.out.print("printf(\"%d\\n\", ");
		expression();
		System.out.println(");");
	} // print()
	
	// if ::= "if" guarded_commands "fi"
	// eif instead of if because if is a keyword in java
	private void eif() {
		System.out.print("\nif");
		mustbe(TK.IF);
		guarded_commands();
		//System.out.println("}");
		mustbe(TK.FI);
	} // eif()
	
	// do ::= "do" guarded_commands "od"
	// edo instead of do because do is a keyword in java
	private void edo() {
		System.out.println("while(1){");
		mustbe(TK.DO);
		System.out.print("if");
		guarded_commands();
		System.out.println("else { break; }");
		System.out.println("}\n");
		mustbe(TK.OD);
	} // edo()
	
	// fa ::= "fa" id ":=" expression "to" expression ["st" expression] commands "af"
	private void fa() {
		System.out.print("for( ");
		mustbe(TK.FA);

		if(symTab.tableContains(token) == -1)
		{
			System.err.println("undeclared variable " + token.string +
				" on line " + token.lineNumber);
			System.exit(1);
		}//if

		// Increment the count of assignments for this line
		symTab.incrementAssignment(token, symTab.getNestingDepth());

		String temp = token.string;

		System.out.print("x_" + token.string);
		mustbe(TK.ID);
		System.out.print(" = ");
		mustbe(TK.ASSIGN);
		expression();
		System.out.print("; x_" + temp);
		System.out.print(" <= ");
		mustbe(TK.TO);
		expression();
		System.out.println("; x_" + temp + "++ )");
		
		// Can have EXACTLY ONE st class
		if (match(TK.ST))	// If there is an optional "st" clause
		{
			System.out.print("if");
			mustbe(TK.ST);
			expression();	// Parse expression following it
		} // if()
		
		commands();
		mustbe(TK.AF);
	} // fa()
	
	// guarded_commands ::= guarded_command { "[]" guarded_command } [ "else" commands ]
	private void guarded_commands() {
		guarded_command();
		
		// Can have INDEFINITEY guarded_commands
		while(match(TK.BOX)) {
			System.out.print("else if");
			mustbe(TK.BOX);
			guarded_command();
		} // while()
		//System.out.println("}");
		// Can have EXACTLY ONE else clause
		if (match(TK.ELSE)) {
			System.out.print("else");
			mustbe(TK.ELSE);
			commands();
		} // if()
	} // guarded_commands()
	
	// guarded_command ::= expression commands
	private void guarded_command() {
		expression();
		commands();
	} // guarded_command()
	
	// commands ::= "->" block
	private void commands() {
		mustbe(TK.ARROW);
		System.out.println("{");
		block();
		System.out.println("}");
	} // commands()
	
	// expression ::= simple [relop simple]
	private void expression() {
		System.out.print("( ");
		simple();
		if (isRelop()) {
			relop();
			simple();
		}
		System.out.print(" )");
	} // expression()
	
	// simple ::= term {addop term}
	private void simple() {
		term();
		
		while(isAddop()) {
			addop();
			term();
		} // while()
	} // simple()
	
	// term ::= factor { multop factor }
	private void term() {
		factor();
	
		while (isMultop()) {
			multop();
			factor();
		} // while()
	} // term()
	
	// factor ::= "(" expression ")" | id | number
	private void factor() {

		if (match(TK.LPAREN)) {
			mustbe(TK.LPAREN);
			System.out.print("( ");
			expression();
			mustbe(TK.RPAREN);
			System.out.print(" )");
		} // chew through expression
		else if (match(TK.ID))  { 
			if(symTab.tableContains(token) == -1)
			{
				System.err.println("undeclared variable " + token.string +
					" on line " + token.lineNumber);
				System.exit(1);
			}//if
			
			// Increment count of usages for this line
			symTab.incrementUsage(token, symTab.getNestingDepth());

			System.out.print("x_" + token.string);
			mustbe(TK.ID);
			
		}	
		else if (match(TK.NUM)) { 
			System.out.print(token.string);
			mustbe(TK.NUM); 
		}
		else                    { error("factor"); }
	} // factor()
	
	// relop ::= "=" | "<" | ">" | "/=" | "<=" | ">="
	private void relop() {
		// Match all possible values
		if (match(TK.EQ)) { 
			mustbe(TK.EQ); 
			System.out.print(" == ");
		}

		else if (match(TK.LT)) { 
			mustbe(TK.LT); 
			System.out.print(" < ");
		} 

		else if (match(TK.GT)) { 
			mustbe(TK.GT); 
			System.out.print(" > ");
		}
		else if (match(TK.NE)) { 
			mustbe(TK.NE); 
			System.out.print(" != ");
		}
		else if (match(TK.LE)) { 
			mustbe(TK.LE); 
			System.out.print(" <= ");
		}
		else if (match(TK.GE)) { 
			mustbe(TK.GE); 
			System.out.print(" >= ");
		} 
		
		// Error if we don't have any of the above
		else { error("relop"); }
	} // relop()
	
	// addop ::= "+" | "-"
	private void addop() {			
		// Match all possible values
		if (match(TK.PLUS))       { 
			mustbe(TK.PLUS);
			System.out.print(" + ");
		}

		else if (match(TK.MINUS)) { 
			mustbe(TK.MINUS); 
			System.out.print(" - ");
		}

		else                      { error("addop"); }
	} // addop()
	
	// multop ::= "*" | "/"
	private void multop() {	
		// Match all possible values
		if (match(TK.TIMES))       { 
			mustbe(TK.TIMES); 
			System.out.print(" * ");
		}

		else if (match(TK.DIVIDE)) { 
			mustbe(TK.DIVIDE); 
			System.out.print(" / ");
		}
		else                       { error("multop"); }
	} // multop()
	
	//==============================================================//
	//                     UTILITY FUNCTIONS                       
	//==============================================================//
	
	// Check if current token is of given type
	// If it is, read next token
	private void mustbe(TK type) {
		if( ! match(type) ) {
			System.err.println( "mustbe: want " + type + ", got " + token);
			error( "missing token (mustbe)" );
		} // if()
		
		scan();
	} // mustbe()
	
	// When we reach an error, report the error and quit parsing
	// Pulled formatting from sample files
	private void error(String message) {
		System.err.println( "can't parse: line " + token.lineNumber + " " + message );
		System.exit(1);
	} // error()

	// Returns true if the current token is in the first set of statement
	private boolean isStatement() {
		if (token.kind != TK.ID) 
			if (token.kind != TK.PRINT) // print	
				if (token.kind != TK.IF)
					if (token.kind != TK.DO)
						if (token.kind != TK.FA)
							return false;
		
		return true;
	} // isStatement()
	
	// Returns true if the current token is a relop
	private boolean isRelop() {
		if (token.kind != TK.EQ)
			if (token.kind != TK.NE)
				if (token.kind != TK.LT)
					if (token.kind != TK.GT)
						if (token.kind != TK.LE)
							if (token.kind != TK.GE)
								return false;
		
		return true;
	} // isRelop()
	
	// Returns true if the current token is a multop
	private boolean isMultop() { 
		if (token.kind != TK.TIMES)
			if (token.kind != TK.DIVIDE)
				return false;
		
		return true;
	} // isMultop()
	
	private boolean isAddop() {
		if (token.kind != TK.PLUS)
			if (token.kind != TK.MINUS)
				return false;
		
		return true;
	} // isAddop()
	
	// Just so we don't have to call "scanner.scan" all the time
	private void scan() { 
		token = scanner.scan(); 
	} // scan()
	
	// Check that current token matches given ID
	private boolean match(TK type) { 
		return token.kind == type;
	} // match()
	
} // Parser()
