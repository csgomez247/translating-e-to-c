import java.util.*;

/**
 * Representation of a variable.
 * Used in SymbolTable.java
 */
public class Variable  {

	private String name;						// Name of Variable
	private int nestingDepth;				// Depth at which first declared
	private int lineNumberDeclared;			// Line on which it's declared
	private ArrayList<Integer> lineNumbers;	// Where it's used
	private Boolean inScope;

	// line number -> times (re)assigned / used
	private Map<Integer, Integer> assignedOn; 
	private Map<Integer, Integer> usedOn;

	/**
	 * Constructor for a Variable object
	 * @param token initializes name, uses, and a starting line number
	 * @param nestingDepth initializes nesting depth of variables declaration
	 */
	public Variable(Token token, int nestingDepth) {
		this.name = token.string;
		this.nestingDepth = nestingDepth;
		this.lineNumberDeclared = token.lineNumber;
		this.inScope = true;
		
		assignedOn = new HashMap<Integer, Integer>();
		usedOn = new HashMap<Integer, Integer>();
		lineNumbers = new ArrayList<Integer>();
		
		// lineNumbers.add(token.lineNumber);
	} // Variable constructor

	/**
	 * Displays the name, number of uses, and locations of use
	 * Formatting was complicated as all hell
	 */
	public void displayUsageInformation() {
		System.err.println(this.name);
		System.err.println("  declared on line " + lineNumberDeclared + " at nesting depth " + nestingDepth);
		
		Integer output;
		
		if (assignedOn.isEmpty())
			System.err.print("  never assigned");
		else
		{
			System.err.print("  assigned to on:");
			
			for (Integer number : lineNumbers) {
				output = assignedOn.get(number);
				
				if (output != null) 
				{	
					System.err.print(" ");
					
					// If done more than once on a line, output count
					System.err.print(number);
					if (output != 1) 
					 	System.err.print("(" + output + ")");	
				} // if
			} // for assignments
		}		
		
		System.out.println();
		
		if (usedOn.isEmpty())
			System.err.print("  never used");
		else
		{
			System.err.print("  used on:");
			
			for (Integer number : lineNumbers) {
				output = usedOn.get(number);
				
				if (output != null) 
				{
					System.err.print(" ");

					// If done more than once on a line, output count
					System.err.print(number);
					if (output != 1) 
					 	System.err.print("(" + output + ")");
				} // if
			} // for used on
		}
		
		System.err.println();
		
	} // displayInfo
	
	/**
	 * Gives the name of the variable
	 * @return the String name of the variable
	 */	
	public String getName() {
		return name; 
	}//name

	/**
	 * Gives nesting depth of a variable
	 * @return nesting depth
	 */
	public int getDepth() {
		return nestingDepth;
	}
	
	/**
	 * Gives validity of variable
	 * @return true if in scope, 0 if out of scope
	 */
	public Boolean isInScope() {
		return inScope;
	}
	
	/**
	 * Sets validity of variable
	 * No return or param
	 */
	public void setOutOfScope() {
		inScope = false;
	}

	/**
	 * Increments the usage of this variable for current line number
	 * @param Line number on which variable was used
	 */
	public void incrementUsage(int lineNumber) {
		// If not alraedy assigned or used on this line, add lineNumber to list
		// Because it can be used more than once on one line, must check this
		if (!(lineNumbers.contains(lineNumber)))
			lineNumbers.add(lineNumber);
			
		// Increment old value by one
		Integer oldValue = usedOn.get((Integer) lineNumber);
		
		// If lineNumber does not map to anything, get returns null
		// If null, then this is first use, set usage to 1
		if (oldValue != null)
			usedOn.put((Integer) lineNumber, oldValue + 1);
		else
			usedOn.put((Integer) lineNumber, 1);
	} // incrementUsage
	
	/**
	 * Increments the assignment of this variable for current line number
	 * @param Line number on which variable was used
	 */
	public void incrementAssignment(int lineNumber) {
		// If not alraedy assigned or used on this line, add lineNumber to list
		// Because it can be used more than once on one line, must check this
		if (!(lineNumbers.contains(lineNumber)))
			lineNumbers.add(lineNumber);
		
		// Increment old value by one
		Integer oldValue = assignedOn.get((Integer) lineNumber);
		
		// If lineNumber does not map to anything, get returns null
		// If null, then this is first use, set assigned to 1
		if (oldValue != null)
			assignedOn.put((Integer) lineNumber, oldValue + 1);
		else
			assignedOn.put((Integer) lineNumber, 1);
	} // incrementAssignment

	/**
	 * Compares two Variable objects
	 * @param var varible to be compared
	 * @return true if the two objects' names are equal, false if not
	 */
	@Override
	public boolean equals(Object o) {

		if(o == null || !(o instanceof Variable))
			return false;
		if(o == this) 
			return true;
		Variable var = (Variable)o;
		return this.getName().equals(var.getName());
	}//equals

	/**
	 * String representation of a Variable
	 * @return Variable's name
	 */
	@Override
	public String toString() {
		return this.getName();
	}
} // Variable