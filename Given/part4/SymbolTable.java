/**
 * Symbol.java
 * Symbol table for e2c translator
 * Keeps each varlist in a stack
 */

import java.util.Collections;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Stack;

/**
 * Symbol table for the E to C Translator.
 * Maintains the VarLists in a stack.
 */
public class SymbolTable {
	// Array of frames arranged as a stack
	private Stack<VarList> frames;
	private LinkedList<Variable> allVars;

	/**
	 * Default, no argument constructor
	 */
	SymbolTable() { 
		frames = new Stack<VarList>();
		allVars = new LinkedList<Variable>();
		
		// this.addFrame(); 
	}//no args contructor
	
	/**
	 * Creates a new frame
	 */
	public void addFrame() { 
		frames.push(new VarList()); 
	}//addFrame()
	
	/**
	 * Adds a variable to the VarList on top of the stack
	 * @param var variable to be added
	 */
	public void addVar(Variable var) {
		if(frames.empty())
			this.addFrame();
		
		(frames.peek()).add(var);
		allVars.addFirst(var);
	}//addVar()

	/**
	 * Determines if frames is empty
	 * @return true if empty, false if not
	 */
	public boolean isEmpty() {
		return frames.empty();
	}

	/**
	 * Deletes the current frame
	 */
	public void popFrame() {
		// Go through and mark proper variables as out of scope
		(frames.peek()).setOutOfScope();
		
		frames.pop(); 
	}//popFrame()

	/**
	 * Checks for the presence of a variable in the Symbol Table
	 * @param token variable name that is searched for
	 * @return 	-1 if Variable is not found, else returns 1-based depth level of Variable relative to top of stack
	 */
	public int tableContains(Token token) {
		VarList varlist = new VarList(new Variable(token, getNestingDepth()));
		return frames.search(varlist);
	}//isInTable
	
	/**
	 * Output detailed information on all variables declared throughout the program
	 * No parameters or the return value necessary
	 */
	public void displayVariables() {		
		Collections.reverse(allVars);
		
		for (Variable var : allVars)
			var.displayUsageInformation();
	} // displayVariables
	
	/**
	 * Increment the usage of the variable specified by the passed string
	 * @param string is name of variable being incremented
	 */
	public void incrementUsage(Token token, int nestingDepth) {
		String varName = token.string;
		int lineNumber = token.lineNumber;
			
		for (Variable var : allVars) {
			if (varName.equals(var.getName()) && var.isInScope())
			{
				var.incrementUsage(lineNumber);
				break;
			}
		}
	} // incrementUsage
	
	/**
	 * Increment the assignments of the variable specified by the passed string
	 * @param string is name of variable being incremented
	 */
	public void incrementAssignment(Token token, int nestingDepth) {
		String varName = token.string;
		int lineNumber = token.lineNumber;
		
		for (Variable var : allVars) {
			if (varName.equals(var.getName()) && var.isInScope())
			{		
				var.incrementAssignment(lineNumber);
				break;
			}
		}
	} // incrementAssignment
	
	/**
	 * Gets and returns the nesting depth of the current frame
	 * @return current size of stack - 1
	 */
	 public int getNestingDepth() {
		return frames.size() - 1;
	 } // getNestingLevel()
} // SymbolTable()
 
