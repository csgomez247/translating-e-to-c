/**
 * VarList.java
 * Keeps track of the variables available in each scope
 */

import java.util.*;

/**
 * Representation of a list of variables in a certain scope
 * There should be a new VarList initialized for every block
 */
public class VarList {
	private ArrayList<Variable> variables;

	/**
	 * No-args constructor for a VarList
	 */
	public VarList() { 
		variables = new ArrayList<Variable>(); 
	}//no args constructor

	/**
	 * Arg constructor that initializes the list with one variable.
	 */
	public VarList(Variable var) {
		variables = new ArrayList<Variable>();
		variables.add(var);
	}
	
	/**
	* Adds a variable to the varlist.
	* @param var the variable that will be added to the VarList as a variable
	*/
	public void add(Variable var) { 
		variables.add(var); 
	} // add()
	
	/**
	 * Checks to see if a variable by the name of 'token' is in the stack of VarLists
	 * @param	token 	the searched token
	 * @return			true if the variable is found, false if the variable is not found
	 */
	public boolean contains(Token token) {
		for (Variable var : variables)
			if (var.getName() == token.string)
				return true;
		return false;
	} // contains()

	/**
	 * Checks to see if a variable is in the stack of VarLists
	 * @param  var variable to be searched
	 * @return
	 */
	public boolean contains(Variable var) {
		for (Variable v : variables)
			if(v.equals(var))
				return true;
		return false;
	}
	
	/**
	 * Checks if two VarLists have a common variable
	 * @param varlsit VarList to be compared
	 * @return false if VarLists don't have a common element, true VarLists have a common element
	 */
	@Override
	public boolean equals(Object o) {
		if(o == null || !(o instanceof VarList))
			return false;
		if(o == this)
			return true;

		VarList varlist = (VarList)o;
		return variables.isEmpty() && variables.contains(varlist.getFirst()) || varlist.contains(this.getFirst());
	}//equals()

	/**
	 * Gets the first Variable in the list
	 * @return the first variable in the list
	 */
	public Variable getFirst() {
		return variables.get(0);
	}//getFirst()

	/**
	 * Sets all variables in this frame to be out of scope
	 * No params, no return value
	 */
	public void setOutOfScope() {
		for (Variable var : variables)
			var.setOutOfScope();
	}

	/**
	 * String representation of a VarList object.
	 * @return String object of all the VarLists' elements
	 */
	@Override
	public String toString() {
		String buffer = new String();
		for(Variable var : variables) {
			buffer.concat(var + ", ");
		}
		return buffer;
	}//toString
}//VarList
